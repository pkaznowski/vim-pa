# Vim config for PythonAnywhere consoles

## Install

Clone this repo and run `config.sh` script inside.

```shell
git clone https://gitlab.com/pkaznowski/vim-pa.git
cd vim-pa
```

Then choose command that you want to run:

```shell
Usage: ./config [command [command]]

Commands:
  ack      will try to install ack
  custom   create ~/.vimrc-custom
  new      create new ~/.vimrc, back up old one if necessary
  plug     install plugins, quit vim
  update   update symlinks
  
```

To run vim with custom runtime use:

```shell
vim -Nu ~/.vimrc-custom
```

The script will create symlinks to files in `vim-pa` in `$HOME/.config/vim` as well.

With first run `vim` will install those plugins:
- gruvbox                 (theme)
- lightline               (status line)
- ack                     (grep but better)
- ctrlp                   (file browsing)
- autopairs               (electric parens)
- surround                (managing boundaries)
- tabular                 (align regexp)
- commentary              (comment region)
- fugitive & gitgutter    (git support)
- python-mode
- emmet                   (html snippets)
- vim-js & vim-jsx-pretty (javascript & React)


## Config structure

Basic settings (`settings.vim`, `plugins.vim`) will be put into `$HOME/.config/vim`.
Plugin specific settings will be put into `themes` or `modes` directories.
Plugin data will be stored automatically in `autoload/plugged`.

`~/.vimrc` will be used only as an init wrapper sourcing paths to chosen plugins/settings files.
 
