if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/autoload/plugged')

Plug 'itchyny/lightline.vim'    " status line with git support
Plug 'fxn/vim-monochrome'       " themes --
Plug 'gruvbox-community/gruvbox'

Plug 'preservim/nerdtree'       " file browsing
Plug 'ctrlpvim/ctrlp.vim'
Plug 'airblade/vim-rooter'
Plug 'mileszs/ack.vim'          " better grep (:Ack)
Plug 'godlygeek/tabular'        " align regexp (:Tabularize /<regex>)
Plug 'tpope/vim-commentary'     " comment region
Plug 'jiangmiao/auto-pairs'     " electric parens
Plug 'tpope/vim-surround'       " surround region, edit boundaries etc.

Plug 'majutsushi/tagbar'        " static analysis
Plug 'vim-syntastic/syntastic'  " linters

Plug 'tpope/vim-fugitive'       " git support
Plug 'airblade/vim-gitgutter'   " git status annotations

Plug 'python-mode/python-mode'  " python
Plug 'yuezk/vim-js'             " js
Plug 'maxmellon/vim-jsx-pretty' " react
Plug 'mattn/emmet-vim'          " html

call plug#end()
