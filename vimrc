source $HOME/.vim/plugins.vim
source $HOME/.vim/settings.vim
source $HOME/.vim/modes/theme.vim
source $HOME/.vim/modes/lightline.vim
source $HOME/.vim/modes/pymode.vim
source $HOME/.vim/modes/syntastic.vim
