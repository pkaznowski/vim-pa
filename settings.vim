""""""""""""""""""""""""""""""""""""""""""""""""""""
"  Some settings that were living here already...  "
""""""""""""""""""""""""""""""""""""""""""""""""""""

set nocompatible    " must be first, changes behaviour of other settings

let mapleader=" "

set t_Co=256        " 256 colors
set fileformat=unix " sane text files
set encoding=utf-8
set tabstop=4       " sane tabs
set shiftwidth=4
set softtabstop=4
set expandtab       " convert all typed tabs to spaces

syntax on " syntax highlighting
autocmd BufEnter * :syntax sync fromstart "make sure highlighting works all the way down long files

set selection=exclusive        " allow cursor to be positioned one char past end of line and apply operations to all of selection including last char
set hidden
set scrolloff=3                " Keep more context when scrolling off the end of a buffer
set whichwrap+=<,>,[,]         " allow cursor keys to go right off end of one line, onto start of next
set backspace=indent,eol,start " allow backspacing over everything in insert mode
set nowrap                     " no line wrapping
set number relativenumber      " line numbers
set nojoinspaces               " when joining lines, don't insert two spaces after punctuation
set ignorecase                 " Make searches case-sensitive only if they contain upper-case characters
set smartcase
set incsearch                  " show search matches as the search pattern is typed
set wrapscan                   " search-next wraps back to start of file
set hlsearch                   " highlight last search matches

" Mappings
" map backspace to dismiss search highlightedness
map <bs> :noh<CR> 
" grep for word under cursor
noremap <Leader>g :grep -rw '<C-r><C-w>' .<CR> 

" aliases for window switching (browser captures ctrl-w)
noremap <C-l> <C-w>l 
noremap <C-h> <C-w>h
noremap <C-k> <C-w>k
noremap <C-j> <C-w>j
nnoremap <leader>q <C-Q> " similarly ctrl-q doesnt work, so use leader-q for block visual mode

set wildmenu       " make tab completion for files/buffers act like bash
set ruler          " display cursor co-ords at all times
set cursorline
set showcmd        " display number of selected chars, lines, or size of blocks.
set showmatch      " show matching brackets, etc, for 1/10th of a second
set matchtime=1

filetype plugin on " enables filetype specific plugins
filetype on        " enables filetype detection

if has("autocmd")
    " Enable file type detection.
    " Use the default filetype settings, so that mail gets 'tw' set to 72, 'cindent' is on in C files, etc.
    " Also load indent files, to automatically do language-dependent indenting.
    filetype plugin indent on
    " When editing a file, always jump to the last known cursor position. Don't do it when the position is invalid or when inside an event handler
    autocmd BufReadPost *
    \ if line("'\"") > 0 && line("'\"") <= line("$") |
    \   exe "normal g`\"" |
    \ endif
else
    " if old vim, set vanilla autoindenting on
    set autoindent
endif " has("autocmd")

set clipboard+=unnamed " enable automatic yanking to and pasting from the selection

set tags=./tags,tags  " places to look for tags files:
" set tags+=./tags;/  " recursively search file's parent dirs for tags file
set tags+=tags;/      " recursively search cwd's parent dirs for tags file

""""""""""""""""""""""""""""""""""""""""""""""""""
" Default settings end here                      "
""""""""""""""""""""""""""""""""""""""""""""""""""

noremap <Leader>V      <C-v>
noremap <Leader>v      :vsplit<CR>
noremap <Leader>h      :split<CR>
noremap <Leader>w      :write<CR>
noremap <Leader>a      :Ack<space>
noremap <Leader>q      :close<CR>
noremap <Leader>b      :CtrlPBuffer<CR>
noremap <Leader>t      :TagbarToggle<CR>
noremap <Leader>c      :Commentary<CR>
noremap <Leader>na     :NERDTreeFocus<CR>
noremap <Leader>nb     :NERDTreeFromBookmark<space>
noremap <Leader>nc     :NERDTreeCWD<CR>
noremap <Leader>nf     :NERDTreeFind<CR>
noremap <Leader>nn     :NERDTreeToggle<CR>
noremap <Leader><C-t>  :Tabularize /

set splitbelow
set splitright
" set autochdir
set noshowmode
set laststatus=2
set noswapfile
set term=screen-256color

autocmd FileType javascript setlocal textwidth=80 tabstop=2 shiftwidth=2 colorcolumn=80
