#!/bin/bash 

vimdir=~/.vim
curdir=$PWD

usage() {
    cat << 'EOF'
Usage: ./config [command [command]]

Commands:
  ack      will try to install ack
  custom   create ~/.vimrc-custom
  new      create new ~/.vimrc, back up old one if necessary
  plug     install plugins, quit vim
  update   update symlinks

EOF
}

link() {
    mkdir -p $vimdir/{autoload,modes}

    for file in *.vim; do
        echo "Linking $file -> $vimdir/$file"
        ln -sf $(pwd)/$file $vimdir/$file
    done

    cd $curdir/modes
    for file in *; do
        echo "Linking $(pwd)/$file -> $vimdir/modes/$file"
        ln -sf $(pwd)/$file $vimdir/modes/$file
    done
}

custom() {
    cus=~/.vimrc-custom
    echo ">> Using custom vimrc, to run vim with this config use:"
    echo ">> vim -Nu ~/.vimrc-custom"
    echo
    ln -sf $curdir/vimrc $cus
    link
}

new() {
    if [ -f ~/.vimrc  ]; then
       echo ">> Old vimrc found, backing up" 
       echo 
       mv ~/.vimrc ~/.vimrc-bak-$(date +"%y%m%d")
    fi
    ln -sf $curdir/vimrc ~/.vimrc
    link
}

ack() {
    if [ $(which ack) ]; then 
        echo Ack already installed
    else
        echo Installing Ack...
        mkdir -p ~/.local/bin
        curl https://beyondgrep.com/ack-v3.3.1 > ~/.local/bin/ack \
            && chmod 0755 ~/.local/bin/ack
    fi
}

plug() { vim +PlugInstall +qall; }

while [ $1 ]; do
    case $1 in
        ack|a    ) ack    ;;
        custom|c ) custom ;;
        new|n    ) new    ;;
        plug|p   ) plug   ;;
        update|u ) link   ;;
        *        ) usage  ;;
    esac
    shift
done

